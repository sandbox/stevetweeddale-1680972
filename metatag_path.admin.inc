<?php

/**
 * @file
 * Provides the Views' administrative interface.
 */

/**
 * Menu callback; metatag_path listing
 */
function metatag_path_list($keys = NULL) {
  // Add the filter form above the overview table.
  $build['path_admin_filter_form'] = drupal_get_form('metatag_path_filter_form', $keys);

  // Render the metatag_path listview with bulk delete form
  $build['bulk_delete_form'] = drupal_get_form('metatag_path_bulk_delete_form', $keys);

  return $build;
}

/**
 * Menu callback; handles pages for creating and editing metatag_path.
 */
function metatag_path_load_admin_form() {
  $prev_path = (isset($_SESSION['last_request_page']) && isset($_SESSION['last_request_page'])) ? $_SESSION['last_request_page'] : '';
  $metatag_path = metatag_path_load($prev_path);
  if (empty($metatag_path)) {
    $metatag_path = array(
      'path_alias' => $prev_path,
      'page_title' => '',
      'meta_keywords' => '',
      'meta_description' => '',
      'id' => NULL,
    );
  }
  $output = drupal_get_form('metatag_path_admin_form', $metatag_path);

  return $output;
}

/**
 * Return a form for metatag_path editing.
 *
 * @ingroup forms
 * @see metatag_path_admin_form_submit()
 * @see metatag_path_admin_form_validate()
 */
function metatag_path_admin_form($form, &$form_state, $form_data = array('path_alias' => '', 'page_title' => '', 'meta_keywords' => '', 'meta_description' => '', 'id' => NULL)) {
  // Define a fieldset.
  $form['metatag_path'] = array(
    '#type' => 'fieldset',
    '#title' => t('metatag_path'),
  );

  $form['metatag_path']['path_alias'] = array(
    '#type' => 'textfield',
    '#title' => t('Path Alias'),
    '#description' => t('Enter the path alias which you want to apply the metatag_path. (ex. /pages/page1)'),
    '#size' => '160',
    '#maxlength' => '255',
    '#default_value' => $form_data['path_alias'],
    '#required' => TRUE,
  );

  $form['metatag_path']['page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Page Title'),
    '#description' => t('Enter the page title.'),
    '#size' => '160',
    '#maxlength' => '255',
    '#default_value' => $form_data['page_title'],
    '#required' => TRUE,
  );

  $form['metatag_path']['meta_keywords'] = array(
    '#type' => 'textarea',
    '#title' => t('Meta Keywords'),
    '#description' => t('Enter the meta keywords in comma separated format.'),
    '#maxlength' => '255',
    '#default_value' => $form_data['meta_keywords'],
    '#required' => FALSE,
  );

  $form['metatag_path']['meta_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Meta Description'),
    '#description' => t('Enter the meta description.'),
    '#size' => '255',
    '#maxlength' => '255',
    '#default_value' => $form_data['meta_description'],
    '#required' => FALSE,
  );



  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  $path_id = isset($form_state['input']['id']) ? $form_state['input']['id'] : $form_data['id'];

  if ($path_id) {
    $form['id'] = array(
      '#type' => 'hidden',
      '#value' => $path_id,
    );
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('metatag_path_admin_form_delete_submit'),
    );
  }

	if (module_exists('token')) {
    $form['token_tree'] = array(
      '#theme' => 'token_tree',
      '#token_types' => array('node', 'term', 'site'),
    );
  }
  else {
    $form['token_tree'] = array(
      '#markup' => '<p>' . t('Enable the <a href="@drupal-token">Token module</a> to view the available token browser.', array('@drupal-token' => 'http://drupal.org/project/token')) . '</p>',
    );
  }

  return $form;
}

/**
  * Validate metatag_path form
  * check if path alias already existed
  */
function metatag_path_admin_form_validate($form, &$form_state) {
  $path_id = isset($form_state['values']['id']) ? $form_state['values']['id'] : NULL;
  if ($form_state['values']['path_alias'] && _metatag_path_exists($form_state['values']['path_alias'], $path_id)) {
    form_set_error('path_alias', t('Path alias has already existed.'));
  }
}

/**
 * Submit function for the metatag_path editing form.
 */
function metatag_path_admin_form_submit($form, &$form_state) {
  global $user;

  if (!empty($form_state['values']['id'])) {
    // update an existing record
    $status = drupal_write_record('metatag_path', $form_state['values'], 'id');

    if ($status === 'SAVED_UPDATED') {
      $message = t("Your metatag_path (id = %id) is updated successfully.", check_plain($form_state['values']['id']));
      drupal_set_message($message);
    }
  }
  else {
    //insert a new record
    $id = db_insert('metatag_path')
      ->fields(array(
          'uid'               => $user->uid,
          'path_alias'        => $form['metatag_path']['path_alias']['#value'],
          'page_title'        => $form['metatag_path']['page_title']['#value'],
          'meta_keywords'     => $form['metatag_path']['meta_keywords']['#value'],
          'meta_description'  => $form['metatag_path']['meta_description']['#value']
        )
      )
      ->execute();
    $message = t("Your metatag_path (id = $id) is created successfully.");
    drupal_set_message($message);
  }

  $form_state['redirect'] = 'admin/config/search/metatags/paths';
}

/**
 * Submit function for the 'Delete' button on the metatag_path editing form.
 */
function metatag_path_admin_form_delete_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $form_state['redirect'] = array('admin/config/search/metatags/paths/delete/' . $form_state['values']['id'], array('query' => $destination));
}

/**
 * Menu callback; confirms deleting a metatag_path
 */
function metatag_path_admin_delete_confirm($form, &$form_state, $metatag_path) {
  if (user_access('administer metatag_path')) {
    $form_state['metatag_path'] = $metatag_path;
    return confirm_form(
      $form,
      t('Are you sure you want to delete metatag_path %title?',
      array('%title' => $metatag_path['path_alias'])),
      'admin/config/search/metatags/paths'
    );
  }
  return array();
}

/**
 * Execute metatag_path deletion
 */
function metatag_path_admin_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $query = db_delete('metatag_path')
      ->condition('id', $form_state['metatag_path']['id'])
      ->execute();
    $form_state['redirect'] = 'admin/config/search/metatags/paths';
  }
}

/**
 * Return a form to filter metatag_paths.
 *
 * @ingroup forms
 * @see metatag_path_filter_form_submit()
 */
function metatag_path_filter_form($form, &$form_state, $keys = '') {
  $form['#attributes'] = array('class' => array('search-form'));
  $form['basic'] = array('#type' => 'fieldset',
    '#title' => t('Filter aliases'),
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['basic']['filter'] = array(
    '#type' => 'textfield',
    '#title' => 'Path alias',
    '#title_display' => 'invisible',
    '#default_value' => $keys,
    '#maxlength' => 128,
    '#size' => 25,
  );
  $form['basic']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
    '#submit' => array('metatag_path_filter_form_submit_filter'),
  );
  if ($keys) {
    $form['basic']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
      '#submit' => array('metatag_path_filter_form_submit_reset'),
    );
  }
  return $form;
}

/**
 * Process filter form submission when the Filter button is pressed.
 */
function metatag_path_filter_form_submit_filter($form, &$form_state) {
  $form_state['redirect'] = 'admin/config/search/metatags/paths/list/' . trim($form_state['values']['filter']);
}

/**
 * Process filter form submission when the Reset button is pressed.
 */
function metatag_path_filter_form_submit_reset($form, &$form_state) {
  $form_state['redirect'] = 'admin/config/search/metatags/paths/list';
}

/**
 * Return a form for bulk delete metatag_path.
 *
 * @ingroup forms
 * @see metatag_path_bulk_delete_form_submit()
 */
function metatag_path_bulk_delete_form($form, $form_state, $keys = '') {

  $query = db_select('metatag_path')->extend('PagerDefault')->extend('TableSort');

  if ($keys) {
    // Replace wildcards with PDO wildcards.
    $query->condition(db_or()
                        ->condition('path_alias', '%' . preg_replace('!\*+!', '%', $keys) . '%', 'LIKE')
                        ->condition('page_title', '%' . preg_replace('!\*+!', '%', $keys) . '%', 'LIKE')
                        ->condition('meta_keywords', '%' . preg_replace('!\*+!', '%', $keys) . '%', 'LIKE')
                        ->condition('meta_description', '%' . preg_replace('!\*+!', '%', $keys) . '%', 'LIKE'));
  }

  $metatag_paths = $query
    ->fields('metatag_path')
    ->limit(50)
    ->execute();

  $header = array(
    'path_alias' => t('Path Alias'),
    'page_title' => t('Page Title'),
    'meta_keywords' => t('Meta Keywords'),
    'meta_description' => t('Meta Description'),
    'operations' => t('Operations')
  );

  $options = array();
  $destination = drupal_get_destination();
  foreach ($metatag_paths as $metatag_path) {

    $operations = array();
    $operations['edit'] = array(
      'title' => t('edit'),
      'href' => "admin/config/search/metatags/paths/edit/$metatag_path->id",
      'query' => $destination,
    );
    $operations['delete'] = array(
      'title' => t('delete'),
      'href' => "admin/config/search/metatags/paths/delete/$metatag_path->id",
      'query' => $destination,
    );

    $options[$metatag_path->id] = array(
      'path_alias' => sprintf('<a href="%s">%s</a>', base_path() . check_plain($metatag_path->path_alias), check_plain($metatag_path->path_alias)),
      'page_title' => sprintf('<a href="%s">%s</a>', base_path() . check_plain($metatag_path->path_alias), check_plain($metatag_path->page_title)),
      'meta_keywords' => check_plain($metatag_path->meta_keywords),
      'meta_description' => check_plain($metatag_path->meta_description),
      'operations' => array(
        'data' => array(
          '#theme' => 'links',
          '#links' => $operations,
          '#attributes' => array('class' => array('links', 'inline', 'nowrap')),
        ),
      )
    );
  }

  $form['table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No metatag_path available. <a href="@link">Add metatag_path</a>.', array('@link' => url('admin/config/search/metatags/paths/add'))),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete Selected'),
    '#submit' => array('metatag_path_bulk_delete_form_submit'),
  );

  return $form;
}

/**
 * Submit function for the 'Delete' button on the metatag_path bulk delete form.
 * @see metatag_path_bulk_delete_form_confirm()
 */
function metatag_path_bulk_delete_form_submit($form, &$form_state) {
  if (count($form['table']['#value']) == 0) {
    drupal_set_message(t('Please select at least one metatag_path.'), 'error');
    return FALSE;
  }

  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }

  $_SESSION['selected_metatag_path'] = $form['table']['#value'];
  $form_state['redirect'] = array('admin/config/search/metatags/paths/delete/selected', array('query' => $destination));

}

/**
 * Menu callback; confirms deleting the selected metatag_path
 * @see metatag_path_bulk_delete_form_confirm_submit()
 */
function metatag_path_bulk_delete_form_confirm($form, &$form_state) {
  if (user_access('administer metatag_path')) {
    return confirm_form(
      $form,
      t('Are you sure you want to delete the selected metatag_path?'),
      'admin/config/search/metatags/paths'
    );
  }
  return array();
}

/**
 * Execute selected metatag_path deletion
 */
function metatag_path_bulk_delete_form_confirm_submit($form, &$form_state) {
  foreach ($_SESSION['selected_metatag_path'] as $metatag_path_id) {
    $query = db_delete('metatag_path')
      ->condition('id', $metatag_path_id)
      ->execute();
    $form_state['redirect'] = 'admin/config/search/metatags/paths';
  }
}
