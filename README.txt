ABOUT
-----

This is very much a sandbox module, but will (hopefully) go on to provide path-
based metatag management for the metatag module.

Currently it's largely a re-named version of the Beanstag sandbox. As such, it
uses its own database table and has no integration with metatag module. I hope
to switch that out to use the metatag module api instead, at which point this
becomes it's own thing and not just a renamed Beanstag.

INTRODUCTION
------------

This module allows to set page titles, meta descriptions and meta tags
according to url/path alias.

INSTALLATION
------------

1. Place the metatag_path module directory in your modules folder (this will
   usually be "sites/all/modules/").

2. Enable the module.

3. A menu link "Update metatag_path" should appear at the top Administrative
   toolbar. If it doesn't, go to Administration » Structure » Menus, the
   "Update metatag_path" menu item should be found under "Management"
   category. Move the menu item to right under "Administration".

4. Click the "Update metatag_path" menu link to update the page title and
   meta tags of the current page.

5. Add/Edit/Delete any metatag_path entries in Administration » Structure
   » Manage metatag_path.
